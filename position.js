const mc = require('minecraft-protocol');

const bot = mc.createClient({
  //host: '95.216.192.50', // kaboom.pw
  host: 'chipmunk.land',
  port: 25565,
  username: "Powitiow",
  version: '1.20.4',
});

inject(bot);

function inject(bot) {
	
  bot.position = { x: NaN, y: NaN, z: NaN, yaw: NaN, pitch: NaN, world: null };

  bot.on('login', (packet) => {
    bot.position.world = packet.worldName;
    // setInterval(() => { console.log(bot.position) }, 500);
  });

  bot.on('respawn', (packet) => { bot.position.world = packet.worldName });
	
  bot.on("position", (packet) => { // im dumb use 350 lines code

    const oldPosition = { ...bot.position };
	
	const { x, y, z, yaw, pitch, flags, teleportId } = packet;

    bot.position.x = flags & 1 ? (bot.position.x + x) : x
    bot.position.y = flags & 2 ? (bot.position.y + y) : y
    bot.position.z = flags & 4 ? (bot.position.z + z) : z
	bot.position.yaw = flags & 8 ? (bot.position.yaw + yaw) : yaw
	bot.position.pitch = flags & 16 ? (bot.position.pitch + pitch) : pitch

    bot.write('teleport_confirm', { teleportId })
  const newPosition = { ...bot.position };
    bot.emit('move', oldPosition, newPosition);
  });

  bot.on("end", () => {
    bot.position = { x: NaN, y: NaN, z: NaN, yaw: NaN, pitch: NaN, world: null };
  });
  
  bot.on('move', (oldPosition, newPosition) => {
	console.log(oldPosition); // NaN value at join
  console.log(newPosition);
  });
}