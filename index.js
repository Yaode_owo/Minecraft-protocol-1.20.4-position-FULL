const mc = require('minecraft-protocol');

const bot = mc.createClient({
  //host: '95.216.192.50', // kaboom.pw
  host: 'chipmunk.land',
  port: 25565,
  username: "Powitiow",
  version: '1.20.4',
});

inject(bot);

function inject(bot) {
	
  bot.position = { x: NaN, y: NaN, z: NaN, yaw: NaN, pitch: NaN, world: null };

  bot.on('login', (packet) => {
    bot.position.world = packet.worldName; // minecraft:overworld
    // setInterval(() => { console.log(bot.position) }, 500);
  });

  bot.on('respawn', (packet) => { // world selfcare real
    bot.position.world = packet.worldName;
    /* world name
    minecraft:overworld
    minecraft:the_nether
    minecraft:the_end
    minecraft:world_flatlands
    */
  });
	
  bot.on("position", (packet) => {

    const { x, y, z, yaw, pitch, flags } = packet;

    switch (flags) { // 0 ~ 31 flags, im cook.
      case 31: // tp @p ~ ~ ~ ~ ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 30: // tp @p x ~ ~ ~ ~
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 29: // tp @p ~ y ~ ~ ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 28: // tp @p x y ~ ~ ~
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 27: // tp @p ~ ~ z ~ ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 26: // tp @p x ~ z ~ ~
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 25: // tp @p ~ y z ~ ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 24: // tp @p x y z ~ ~
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 23: // tp @p ~ ~ ~ yaw ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 22: // tp @p x ~ ~ yaw ~
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 21: // tp @p ~ y ~ yaw ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 20: // tp @p x y ~ yaw ~
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 19: // tp @p ~ ~ z yaw ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 18: // tp @p x ~ z yaw ~
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 17: // tp @p ~ y z yaw ~
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 16: // tp @p x y z yaw ~
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: z,
          yaw: yaw,
          pitch: bot.position.pitch + pitch
        };
        break;
      case 15: // tp @p ~ ~ ~ ~ pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 14: // tp @p x ~ ~ ~ pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 13: // tp @p ~ y ~ ~ pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 12: // tp @p x y ~ ~ pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: bot.position.z + z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 11: // tp @p ~ ~ z ~ pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 10: // tp @p x ~ z ~ pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 9: // tp @p ~ y z ~ pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 8: // tp @p x y z ~ pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: z,
          yaw: bot.position.yaw + yaw,
          pitch: pitch
        };
        break;
      case 7: // tp @p ~ ~ ~ yaw pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 6: // tp @p x ~ ~ yaw pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 5: // tp @p ~ y ~ yaw pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 4: // tp @p x y ~ yaw pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: bot.position.z + z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 3: // tp @p ~ ~ z yaw pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: bot.position.y + y,
          z: z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 2: // tp @p x ~ z yaw pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: bot.position.y + y,
          z: z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 1: // tp @p ~ y z yaw pitch
        bot.position = {
          ...bot.position,
          x: bot.position.x + x,
          y: y,
          z: z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      case 0: // tp @p x y z yaw pitch
        bot.position = {
          ...bot.position,
          x: x,
          y: y,
          z: z,
          yaw: yaw,
          pitch: pitch
        };
        break;
      default:
        console.log('Found Other Packet Flags Id.\n' + JSON.stringify(packet, null, 2));
    }
	console.log(bot.position);
    bot.write("teleport_confirm", { teleportId: packet.teleportId });
  });

  bot.on("end", () => {
    bot.position = { x: NaN, y: NaN, z: NaN, yaw: NaN, pitch: NaN, world: null };
  });
}